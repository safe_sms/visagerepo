package com.fso.visagealpha.Service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.fso.safe_sms.R;

import java.util.ArrayList;

public class MusicService extends Service implements MediaPlayer.OnCompletionListener{

    MediaPlayer mediaPlayer;
    IBinder binder=new LocalBinder();
    int position=0;
    ArrayList<Integer> songList=new ArrayList<>();
    private boolean repeat=false;

    @Override
    public void onCreate() {

        buildRawList();

        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        mediaPlayer = MediaPlayer.create(getApplicationContext(), songList.get(position));
        mediaPlayer.setOnCompletionListener(this);

        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mediaPlayer.setLooping(repeat);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if(!repeat && position==songList.size()-1) {
            position = 0;
        }
        else if(!repeat)
            position++;

        sendBroadcast(new Intent("UPDATE_PLAYER_UI"));
    }


    public void playPause() {


        if(!mediaPlayer.isPlaying())
            mediaPlayer.start();
        else
            mediaPlayer.pause();
    }


    public void next() {
        mediaPlayer.stop();
        mediaPlayer.reset();

        mediaPlayer=MediaPlayer.create(getApplicationContext(),songList.get(position));
        mediaPlayer.setLooping(repeat);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.start();
    }


    public void back() {

        mediaPlayer.stop();
        mediaPlayer.reset();

        mediaPlayer=MediaPlayer.create(getApplicationContext(),songList.get(position));
        mediaPlayer.setLooping(repeat);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.start();

    }

    public void seek(int seconds){
        mediaPlayer.seekTo(seconds);
    }

    private void buildRawList(){
        songList.add(R.raw.song1);
        songList.add(R.raw.song2);
        songList.add(R.raw.song3);
    }

    public void setRepeat(boolean looping){
        repeat=looping;
        mediaPlayer.setLooping(repeat);
    }
    public int getCurrentPosition(){
        return mediaPlayer.getCurrentPosition();
    }

    public int getFileDuration(){
        return (mediaPlayer.getDuration()/1000);
    }

    public class LocalBinder extends Binder {
        public MusicService getServerInstance() {
            return MusicService.this;
        }
    }

    public void setPosition(int position){
        this.position=position;
    }
}
