package com.fso.visagealpha.Fragments;

import static android.content.Context.BIND_AUTO_CREATE;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.fso.safe_sms.R;
import com.fso.visagealpha.Service.MusicService;
import com.fso.visagealpha.SongData.SongDataHolder;
import com.fso.visagealpha.UserData.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Random;

public class MediaPlayerFrag extends Fragment implements View.OnClickListener, ServiceConnection,
        SeekBar.OnSeekBarChangeListener {
    ImageView backButton,playButton,nextButton;
    boolean isPlaying=true;
    int position=0;
    int volProgress;
    int songId;
    ArrayList<SongDataHolder> songList;
    ArrayList<Drawable> albumArtList;
    TextView title;
    ImageView albumImage;
    Intent musicServiceIntent;
    boolean bounded, repeating, randomize;
    Button random, repeat;
    BroadcastReceiver receiver;
    Runnable runnable;
    SeekBar timeSeeker;
    SeekBar volSeekbar;
    Handler handler;
    UpdateUI updateReceiver;
    AudioManager audioManager;
    User user;
    MediaPlayer player;


    public MediaPlayerFrag(){}

    // CONSTRUCTOR USED FOR KEYSTREAM PULLS VIA USER
    public MediaPlayerFrag(User user){

        this.user=user;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String song=("song"+1);
        int songId=getResources().getIdentifier(song, "raw", getContext().getPackageName());
        player=MediaPlayer.create(getContext(), songId);

        timeSeeker =view.findViewById(R.id.seeker);
        timeSeeker.setOnSeekBarChangeListener(this);

        volSeekbar = view.findViewById(R.id.volume);
        volSeekbar.setOnSeekBarChangeListener(this);

        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        volSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volSeekbar.setProgress(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        repeating=false;
        randomize=false;

        buildSongList();
        buildArtList();

        playButton=view.findViewById(R.id.playButton);
        playButton.setOnClickListener(this);

        backButton=view.findViewById(R.id.backButton);
        backButton.setOnClickListener(this);

        nextButton=view.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(this);

        repeat=view.findViewById(R.id.repeatButton);
        repeat.setOnClickListener(this);

        random=view.findViewById(R.id.randomButton);
        random.setOnClickListener(this);

        albumImage=view.findViewById(R.id.albumArt);

        title=view.findViewById(R.id.songTitle);
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter("android.intent.MAIN");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action=intent.getStringExtra("action");

                if(action.equalsIgnoreCase("PAUSEPLAY")) {
                    playPause();
                }

                else if(action.equalsIgnoreCase("SKIP")) {
                    next();
                }

                else if(action.equalsIgnoreCase("BACK")) {
                    back();
                }

            }
        };

    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            bounded = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            bounded = true;
            MusicService.LocalBinder mLocalBinder = (MusicService.LocalBinder) service;
        }
    };

    @Override
    public void onClick(View v) {
        if(v.getId()==playButton.getId()) {

            playPause();
        }
        else if(v.getId()==repeat.getId()){

            if(!repeating) {
                random.setVisibility(View.GONE);
                repeat.setText(R.string.repeatString);
                repeating=true;
            }
            else{
                random.setVisibility(View.VISIBLE);
                repeat.setText(R.string.randomString);
                repeating=false;
            }

        }
        else if(v.getId()==random.getId()){
            if(!randomize){
                randomize=true;

                repeat.setVisibility(View.GONE);
                random.setText(R.string.randomOn);
            }

            else{
                randomize=false;

                repeat.setVisibility(View.VISIBLE);
                random.setText(R.string.randomOff);
            }

        }
        else if(v.getId()==backButton.getId())
            back();

        else
            next();
    }

    public void playPause() {

        if(randomize) {
            getRandomPosition();
        }

        title.setText(songList.get(position).getTitle());
        albumImage.setImageDrawable(albumArtList.get(position));

        startSong();

        if(isPlaying) {
            playButton.setImageResource(R.drawable.ic_baseline_pause_circle_outline_24);
            isPlaying=false;

        }
        else {
            playButton.setImageResource(R.drawable.ic_baseline_play_arrow_24);
            isPlaying = true;

        }
    }

    private void startSong() {

        if(player.isPlaying())
            player.pause();
        else
            player.start();

    }

    public void next() {

        if(randomize) {
            getRandomPosition();
        }

        else {
            if (position == 2)
                position = 0;
            else
                position++;

            playNext();

        }

        title.setText(songList.get(position).getTitle());
        albumImage.setImageDrawable(albumArtList.get(position));

    }

    private void playNext() {

        if(player.isPlaying())
            player.release();

        int song=getResources().getIdentifier(("song"+(position+1)), "raw", getContext().getPackageName());

        player=MediaPlayer.create(getContext(), song);
        player.start();

    }

    public void back() {

        if(randomize) {
            getRandomPosition();
        }

        else {
            if (position == 0)
                position = 2;
            else
                position--;

            playNext();

        }

        title.setText(songList.get(position).getTitle());
        albumImage.setImageDrawable(albumArtList.get(position));
    }

    private void buildSongList(){
        songList=new ArrayList<>();

        SongDataHolder tmpHolder=new SongDataHolder("The only thing they fear is you", "Mick Gordon");
        songList.add(tmpHolder);

        SongDataHolder tmpHolder1=new SongDataHolder("BFG Division", "Mick Gordon");
        songList.add(tmpHolder1);

        SongDataHolder tmpHolder2=new SongDataHolder("Rip And Tear", "Mick Gordon");
        songList.add(tmpHolder2);

    }

    private void buildArtList(){
        albumArtList=new ArrayList<>();
        Drawable album1, album2, album3;

        album1= ResourcesCompat.getDrawable(getResources(),R.drawable.album1,null);
        albumArtList.add(album1);

        album2= ResourcesCompat.getDrawable(getResources(),R.drawable.album2,null);
        albumArtList.add(album2);

        album3= ResourcesCompat.getDrawable(getResources(),R.drawable.album3,null);
        albumArtList.add(album3);


    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    public void getRandomPosition() {

        Random random=new Random();
        position= random.nextInt(3);

    }



    private void startSeekbar(){


        runnable= this::startSeekbar;
        handler=new Handler();
        handler.postDelayed(runnable,1000);
    }

    // NOT NEEDED FOR NOW
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        switch (seekBar.getId()){

            case R.id.volume:

                volProgress=progress;

                break;

        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

        switch (seekBar.getId()) {

            case R.id.seeker:

                if (handler != null)
                    handler.removeCallbacks(runnable);

                break;

        }

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        switch (seekBar.getId()){
            case R.id.seeker:

                if(handler!=null)
                    handler.removeCallbacks(runnable);


                startSeekbar();

                break;

            case R.id.volume:

                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volProgress, 0);

                break;
        }
    }

    public class UpdateUI extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {



            if(intent.getAction().equals("UPDATE_PLAYER_UI"))
                next();


        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.media_layout, container, false);

        return view;
    }
}