package com.fso.visagealpha.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.fso.safe_sms.R;
import com.fso.visagealpha.UserData.User;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class StartFragment extends Fragment implements View.OnClickListener {

    EditText passwordET,emailET;
    Button registerButton, loginButton;
    FirebaseAuth auth;
    User user;

    public StartFragment(){

        // Configure Google Sign In
        FirebaseApp.initializeApp(getContext());
        auth=FirebaseAuth.getInstance();

    }

    private void pushUserRegistration(String pass, String email) {

        auth=FirebaseAuth.getInstance();

        auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                Toast.makeText(getContext(),"DATA PUSHED", Toast.LENGTH_SHORT)
                        .show();

                user = new User(email,pass);

                FirebaseDatabase.getInstance()
                        .getReference("users")
                        .child(Objects.requireNonNull(FirebaseAuth.getInstance()
                                .getCurrentUser())
                        .getUid())
                        .setValue(user)
                        .addOnCompleteListener(task1 -> {
                            if(task1.isSuccessful())
                                Toast.makeText(getContext(), "Worked",Toast.LENGTH_LONG)
                                    .show();

                            else
                                Log.wtf("ERROR: ", task1.getException());
                        });

            }

            else {

                Log.wtf("StartFragError: ", Objects.requireNonNull(task.getException()).toString());


            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        passwordET= requireView().findViewById(R.id.passwordEDED);
        emailET= requireView().findViewById(R.id.emailED);
        registerButton=getView().findViewById(R.id.registerButton);
        loginButton=getView().findViewById(R.id.loginButton);

        registerButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registration_frag_layout, container, false);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.registerButton:
                validateInput();
                break;

            case R.id.loginButton:
                loginUser();
                break;
        }

    }

    private void loginUser() {

        auth=FirebaseAuth.getInstance();

        String email=emailET.getText().toString();
        String password=passwordET.getText().toString();

        auth.signInWithEmailAndPassword(email,
                password).addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        Toast.makeText(getContext(),"IM IN!!!",Toast.LENGTH_LONG)
                                .show();

                        DatabaseReference databaseReference=FirebaseDatabase
                                .getInstance()
                                .getReference("ks");


                        getParentFragmentManager().beginTransaction().replace(R.id.fragmentFrame,
                                new UserScrape(user))
                                .addToBackStack(null)
                                .commit();

                    }

                    else{
                        TextView errorText= requireView().findViewById(R.id.loginError);
                        errorText.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void validateInput() {

        if(passwordET.getText().toString().trim().isEmpty()){
            passwordET.setError("PLEASE INPUT A PASSWORD");
            return;
        }

        if(passwordET.getText().toString().trim().length()<6){
            passwordET.setError("PASSWORD MUST BE MORE THAN 5 CHARACTERS");
            return;
        }

        if(emailET.getText().toString().trim().isEmpty()){
            emailET.setError("PLEASE INPUT AN EMAIL");
            return;
        }

        String tmpEmail=emailET.getText().toString().trim();

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(tmpEmail).matches()){
            emailET.setError("INVALID EMAIL, TRY AGAIN");
            return;
        }

        pushUserRegistration(passwordET.getText().toString().trim(),
                emailET.getText().toString().trim());
    }

}
