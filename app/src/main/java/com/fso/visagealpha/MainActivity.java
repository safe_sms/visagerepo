package com.fso.visagealpha;

import android.Manifest;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.fso.safe_sms.R;
import com.fso.visagealpha.Fragments.StartFragment;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPerms();
        popStartFrag();
    }

    private void popStartFrag() {

        Objects.requireNonNull(getSupportActionBar()).hide();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFrame,new StartFragment())
                .commit();
    }


    private void requestPerms(){

        String[] permissions={Manifest.permission.RECEIVE_SMS,Manifest.permission.SEND_SMS};

        ActivityCompat.requestPermissions(this, permissions,1);
    }
}