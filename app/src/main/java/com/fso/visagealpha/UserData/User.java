package com.fso.visagealpha.UserData;

public class User {
    public String email,password, keystream, make, model, year;

    public User(String email, String password) {
        this.email = email;
        this.password = password;

        make="DEFAULT";
        model="DEFAULT";
        year="DEFAULT";
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getYear() {
        return year;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
