package com.fso.visagealpha.SongData;

public class SongDataHolder {

    private final String title;
    private final String artist;

    public SongDataHolder(String title, String artist) {
        this.title = title;
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

}
